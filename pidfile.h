#ifndef _PIDFILE_H_
#define _PIDFILE_H_

#include <sys/types.h>

int pidfile_create(const char *filename);
int pidfile_remove(const char *filename);

pid_t pidfile_check(const char *filename, int remove_stale);

#endif // _PIDFILE_H_
