DESTDIR =
BINARY_DIR = /usr/local/bin
CONFIG_DIR = /usr/local/etc
LOG_DIR = /var/log
PID_DIR = /var/run

# ############################

SRC = $(wildcard *.c)
TARGET = bcastfwd
BUILD_DIR = build
CFLAGS = -O2 -g -pipe -Wall -Wno-unused-result
CFLAGS += -MMD -MF $(BUILD_DIR)/$(*D)/$(*F).d
LDFLAGS =

# ############################

ifeq ($(strip $(wildcard $(DESTDIR)$(CONFIG_DIR)/$(TARGET).conf)),)
	NEWCONF=$(TARGET).conf
else
	NEWCONF=$(TARGET).conf.dist
endif

# ############################

all: $(TARGET)

$(TARGET): $(patsubst %,$(BUILD_DIR)/%, $(SRC:.c=.o))
	@echo " Linking file:  $@"
	@$(CC) $(LDFLAGS) $^ -o $@

$(BUILD_DIR)/%.o: %.c $(MAKEFILE_LIST)
	@echo " Building file: $<"
	@$(shell test -d $(BUILD_DIR)/$(*D) || mkdir -p $(BUILD_DIR)/$(*D))
	@$(CC) $(CFLAGS) -o $@ -c $<

install: $(TARGET)
	install -D -m 755 -s $(TARGET) $(DESTDIR)$(BINARY_DIR)/$(TARGET)
	install -D -m 644 $(TARGET).conf $(DESTDIR)$(CONFIG_DIR)/$(NEWCONF)
	sed -i -e "s:^logfile .*$$:logfile $(LOG_DIR)/$(TARGET).log:" \
		-e "s:^pidfile .*$$:pidfile $(PID_DIR)/$(TARGET).pid:" \
		$(DESTDIR)$(CONFIG_DIR)/$(NEWCONF)
	install -d -m 755 $(DESTDIR)$(LOG_DIR)

clean:
	rm -rf $(BUILD_DIR) $(TARGET)

include $(shell find $(BUILD_DIR) -name \*.d 2> /dev/null)
