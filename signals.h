#ifndef _SIGNALS_H
#define _SIGNALS_H

#define SIG_DEFAULT	0x00
#define SIG_IGNORE	0x01

int signal_remove_callback(int signum, int type);
int signal_add_callback(int signum, void (*callback)(void *privdata), void *privdata);

int signal_init(void);

#endif // _SIGNALS_H
