/***************************************************************************
 *   Copyright (C) 12/2010 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <getopt.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <signal.h>

#include "configfile.h"
#include "event.h"
#include "logging.h"
#include "pidfile.h"
#include "signals.h"

#define DEFAULT_CONFIG "bcastfwd.conf"
#define DEFAULT_LOGFILE "bcastfwd.log"
#define DEFAULT_PIDFILE "bcastfwd.pid"

int bcast_init(void);
int bcast_close(void);

static struct option opts[] = {
	{"config",	1, 0, 'c'},
	{"debug",	0, 0, 'd'},
	{"help",	0, 0, 'h'},
	{0, 0, 0, 0}
};

static int restart_var;

static void trigger_restart(void *privdata)
{
	int *restart = (int *)privdata;
	*restart = 1;
}

int check_restart(int *maxfd, void *readfds, void *writefds, struct timeval *timeout, void *privdata)
{
	int *restart = (int *)privdata;
	if (*restart == 1) {
		*restart = 0;
		return 1;
	}

	return 0;
}

int main(int argc, char *argv[])
{
	char *config = DEFAULT_CONFIG;
	int code, arg = 0, debug = 0;

	do {
		code = getopt_long(argc, argv, "c:dh", opts, &arg);

		switch (code) {
		case 'c':	/* config */
				config = optarg;
				break;

		case 'd':	/* debug */
				debug = 1;
				break;

		case 'h':	/* help */
				printf("Usage: bcastfwd [options]\n"
					"Options: \n"
					"  --config       -c  configfile  use this configfile\n"
					"  --debug        -d              do not fork and log to stderr\n"
					"  --help         -h              this help\n"
					"\n");
				exit(0);
				break;

		case '?':	/* error */
				exit(1);
				break;

		default:	/* unknown / all options parsed */
				break;
		}
	} while (code != -1);

	/* parse config file */
	if (config_parse(config) < 0)
		exit(1);

	if (!debug) {
		/* check pidfile */
		const char *pidfile = config_get_string("global", "pidfile", DEFAULT_PIDFILE);
		if (pidfile_check(pidfile, 1) != 0) {
			log_print(LOG_ERROR, "bcastfwd already running");
			exit(1);
		}

		/* start logging */
		const char *logfile = config_get_string("global", "logfile", DEFAULT_LOGFILE);
		if (log_init(logfile) < 0)
			exit(1);

		/* zum daemon mutieren */
		if (daemon(-1, 0) < 0) {
			log_print(LOG_ERROR, "failed to daemonize");
			exit(1);
		}

		/* create pidfile */
		if (pidfile_create(pidfile) < 0) {
			log_print(LOG_ERROR, "failed to create pidfile %s", pidfile);
			exit(1);
		}
	}

	signal_init();
	signal_add_callback(SIGHUP, trigger_restart, (void *)&restart_var);

	log_print(LOG_EVERYTIME, "bcastfwd started (pid:%d)", getpid());

	while (1) {
		if (bcast_init())
			break;

		/* exited on restart / SIGUSR1 */
		event_loop(check_restart, NULL, (void *)&restart_var);

		bcast_close();

		config_free();

		if (config_parse(config) < 0)
			break;

		const char *logfile = config_get_string("global", "logfile", DEFAULT_LOGFILE);
		if (!debug && log_init(logfile) < 0)
			break;

		log_print(LOG_EVERYTIME, "bcastfwd restarted (pid:%d) ", getpid());
	}

	return 0;
}
