#ifndef _SOCKADDR_H_
#define _SOCKADDR_H_

#include <netinet/in.h>

int parse_sockaddr(const char *addr, struct sockaddr_in *sa);
int parse_subnet(const char *addr, struct in_addr *net, struct in_addr *mask);

int get_sockaddr(char *buf, int size, struct sockaddr_in *addr);
char * get_sockaddr_buf(struct sockaddr_in *addr);

int same_sockaddr(struct sockaddr_in *a, struct sockaddr_in *b);

#endif /* _SOCKADDR_H_ */
