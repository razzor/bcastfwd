#ifndef _LOGGING_H_
#define _LOGGING_H_

#define	LOG_EMERG	0	/* system is unusable */
#define	LOG_ALERT	1	/* action must be taken immediately */
#define	LOG_CRIT	2	/* critical conditions */
#define	LOG_ERR		3	/* error conditions */
#define	LOG_WARNING	4	/* warning conditions */
#define	LOG_NOTICE	5	/* normal but significant condition */
#define	LOG_INFO	6	/* informational */
#define	LOG_DEBUG	7	/* debug-level messages */

#define LOG_EVERYTIME	LOG_EMERG
#define LOG_ERROR	LOG_ERR
#define LOG_WARN	LOG_WARNING

int log_init(const char *logfile);
void log_close(void);
void log_setprio(int prio);

int log_print(int prio, const char *fmt, ... );

#endif /* _LOGGING_H_ */
